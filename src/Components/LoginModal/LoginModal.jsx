import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { setUserInfoStore } from "../../Redux/userSlice";
import { localService } from "../../Services/local.service";
import { UnorderedListOutlined } from "@ant-design/icons";
import { Button, Dropdown, Space } from "antd";

function LoginModal() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  let userInfo = localService.get();
  const handleDangXuat = () => {
    localService.remove();
    dispatch(setUserInfoStore(null));
    navigate("/");
  };
  const renderLoginModal = () => {
    if (userInfo) {
      let itemsLoggedin = [
        {
          label: (
            <p
              onClick={() => handleDangXuat}
              className="text-xl font-bold text-rose-500"
            >
              {userInfo.user.name}
            </p>
          ),
          key: "0",
        },
        {
          label: (
            <span
              onClick={() => navigate("/user-info")}
              className="hover:text-rose-500"
            >
              Thông tin cá nhân
            </span>
          ),
          key: "1",
        },
        {
          type: "divider",
        },
        {
          label: (
            <Button
              className="border-rose-500 text-rose-500"
              onClick={handleDangXuat}
            >
              Đăng xuất
            </Button>
          ),
          key: "2",
        },
      ];
      return (
        <Dropdown menu={{ items: itemsLoggedin }} trigger={["click"]}>
          <div onClick={(e) => e.preventDefault()}>
            <Space className="md:border-2 md:rounded-full px-2 py-1 flex items-center">
              <img
                src={userInfo.user.avatar}
                className="w-12 h-12 rounded-full"
                alt=""
              />
              <UnorderedListOutlined className="text-2xl hidden md:block" />
            </Space>
          </div>
        </Dropdown>
      );
    } else {
      let itemsNotLogin = [
        {
          label: (
            <span
              onClick={() => navigate("/user-info")}
              className="hover:text-rose-500"
            >
              Thông tin cá nhân
            </span>
          ),
          key: "0",
        },
        {
          type: "divider",
        },
        {
          label: (
            <span
              onClick={() => navigate("/login")}
              className="hover:text-rose-500"
            >
              Đăng nhập
            </span>
          ),
          key: "1",
        },
        {
          label: (
            <span
              onClick={() => navigate("/signup")}
              className="hover:text-rose-500"
            >
              Đăng kí
            </span>
          ),
          key: "2",
        },
      ];
      return (
        <Dropdown menu={{ items: itemsNotLogin }} trigger={["click"]}>
          <div onClick={(e) => e.preventDefault()}>
            <Space className="md:border-2 md:rounded-full px-2 py-1 flex items-center">
              <i className="fa fa-user-circle text-3xl px-1 rounded-full"></i>
              <UnorderedListOutlined className="text-2xl hidden md:block" />
            </Space>
          </div>
        </Dropdown>
      );
    }
  };
  return <div>{renderLoginModal()}</div>;
}

export default LoginModal;
