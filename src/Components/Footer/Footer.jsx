import React from "react";

function Footer() {
  return (
    <div className="container mx-auto">
      <div className=" container mx-auto flex flex-col mt-10 pt-10 border-t-2">
        <div className="container mx-auto flex flex-col md:flex-row justify-between">
          <div className="md:w-1/4 p-3 text-left">
            <h3 className="text-md font-bold uppercase pb-3">Giới Thiệu</h3>
            <div className="flex flex-col text-sm gap-3">
              <div>Phương thức hoạt động của Airbnb</div>
              <div>Trang tin tức</div>
              <div>Nhà đầu tư</div>
              <div>Airbnb Plus</div>
              <div>Airbnb Luxe</div>
              <div>HotelTonight</div>
              <div>Airbnb for work</div>
              <div>Nhờ có host, mọi điều đều có thể</div>
              <div>Cơ hội nghề nghiệp</div>
              <div>Thư của nhà sáng lập</div>
            </div>
          </div>
          <div className="md:w-1/4 p-3 text-left">
            <h3 className="text-md font-bold uppercase pb-3">Cộng Đồng</h3>
            <div className="flex flex-col text-sm gap-3">
              <div>Sự đa dạng và cảm giác thân thuộc</div>
              <div>Tiện nghi phù hợp cho người khuyết tật</div>
              <div>Đối tác liên kết Airbnb</div>
              <div>Chỗ ở cho tuyến đầu</div>
              <div>Lượt giới thiệu của khách</div>
              <div>Airbnb.org</div>
            </div>
          </div>
          <div className="md:w-1/4 p-3 text-left">
            <h3 className="text-md font-bold uppercase pb-3">Đón Tiếp Khách</h3>
            <div className="flex flex-col text-sm gap-3">
              <div>Cho thuê nhà</div>
              <div>Tổ chức trải nghiệm trực tuyến</div>
              <div>Tổ chức trải nghiệm</div>
              <div>Đón tiếp khách có trách nhiệm</div>
              <div>Trung tâm tài nguyên</div>
              <div>Trung tâm cộng đồng</div>
            </div>
          </div>
          <div className="md:w-1/4 p-3 text-left">
            <h3 className="text-md font-bold uppercase pb-3">Hỗ Trợ</h3>
            <div className="flex flex-col text-sm gap-3">
              <div>Biện pháp ứng phó với đại dịch COVID-19 của chúng tôi</div>
              <div>Trung tâm trợ giúp</div>
              <div>Các tuỳ chọn huỷ</div>
              <div>Hỗ trợ khu dân cư</div>
              <div>Tin cậy và an toàn</div>
            </div>
          </div>
        </div>
        <hr />
      </div>
      <div className="flex justify-between text-sm py-3">
        <div className="flex justify-center w-full md:w-1/2 md:justify-start">
          <span>
            <i className="fa fa-copyright pr-3"></i>
            2021 Airbnb, Inc. All rights reserved.
          </span>
          <ul className="hidden justify-evenly md:flex">
            <li className="px-2">Quyền riêng tư</li>
            <li className="px-2">Điều khoản</li>
            <li className="px-2">Sơ đồ trang web</li>
          </ul>
        </div>
        <div className="w-1/4 md:flex justify-between hidden">
          <div>
            <i className="fa fa-globe px-2"></i>
            <span className="underline">Tiếng Việt(VN)</span>
          </div>
          <div>
            <span className="px-2">$</span>
            <span className="underline">USD</span>
          </div>
          <ul className="flex justify-between w-1/4">
            <li>
              <i className="fab fa-facebook-f"></i>
            </li>
            <li>
              <i className="fab fa-twitter"></i>
            </li>
            <li>
              <i className="fab fa-instagram"></i>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Footer;
