import React from "react";
import { useSelector } from "react-redux";
import { SyncLoader } from "react-spinners";

export default function Spinner() {
  let { isLoading } = useSelector((state) => {
    return state.spinner;
  });
  return isLoading ? (
    <div className="fixed w-screen flex h-screen items-center justify-center bg-black top-0 left-0 z-50">
      <SyncLoader color="#ff385c" />
    </div>
  ) : (
    <></>
  );
}
