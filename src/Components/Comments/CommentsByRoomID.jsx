import { message } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { commentService } from "../../Services/comments.service";
import Heart from "../RatingHeart/Heart";
import { Input, Button, Modal } from "antd";
import LogInPage from "../../Pages/LogInPage/LogInPage";

function CommentsByRoomID() {
  const { TextArea } = Input;
  const { roomID, userInfo } = useSelector((state) => state.user);
  const [commentsData, setCommentsData] = useState([]);
  const [userComment, setUserComment] = useState("");
  const [rating, setRating] = React.useState(0);
  const [selection, setSelection] = React.useState(0);

  const hoverOver = (event) => {
    let val = 0;
    if (event && event.target && event.target.getAttribute("data-heart-id"))
      val = event.target.getAttribute("data-heart-id");
    setSelection(val);
  };
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const fetchComments = async () => {
    try {
      let res = await commentService.getCommentsByRoomID(String(roomID));
      // console.log(res.data.content);
      setCommentsData(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const renderComments = () => {
    return commentsData.map((person, index) => {
      return (
        <div key={index}>
          <div className="flex items-center py-3">
            <div className="flex">
              {person.avatar ? (
                <img
                  src={person.avatar}
                  alt=""
                  className="w-16 h-16 rounded-full"
                />
              ) : (
                <i className="fa fa-user-circle w-16 h-16 text-6xl text-slate-300"></i>
              )}
            </div>
            <div className="flex flex-col px-5">
              <h3 className="font-medium">{person.tenNguoiBinhLuan}</h3>
              <p className="text-slate-500">{person.ngayBinhLuan}</p>
            </div>
          </div>
          <div className="bg-slate-100 rounded p-1">
            <p className="text-slate-800">{person.noiDung}</p>
          </div>
        </div>
      );
    });
  };
  const renderRating = () => {
    return (
      <div
        onMouseOut={() => hoverOver(null)}
        onClick={(e) =>
          setRating(e.target.getAttribute("data-heart-id") || rating)
        }
        onMouseOver={hoverOver}
      >
        {Array.from({ length: 5 }, (v, i) => (
          <Heart
            heartId={i + 1}
            key={`heart_${i + 1}`}
            marked={selection ? selection >= i + 1 : rating >= i + 1}
          />
        ))}
      </div>
    );
  };
  const handleUserComment = (e) => {
    setUserComment(e.target.value);
  };
  const handleAddCommentButton = async () => {
    if (userInfo) {
      let data = {
        id: 0,
        maPhong: roomID,
        maNguoiBinhLuan: userInfo.user.id,
        ngayBinhLuan: String(new Date()),
        noiDung: userComment,
        saoBinhLuan: rating,
      };
      if (userComment.trim() !== "") {
        try {
          let res = await commentService.postComment(data);
          console.log(res.data);
          message.success(res.data.message);
          fetchComments();
        } catch (err) {
          console.log(err);
        }
      } else {
        return;
      }
    } else {
      message.error("Bạn chưa đăng nhập, đăng nhập để tiếp tục!");
      showModal();
      return;
    }
  };
  useEffect(() => {
    fetchComments();
  }, []);
  return (
    <div>
      <div className="border-t-2 grid md:grid-cols-2 gap-10 pt-10">
        {renderComments()}
      </div>
      <div className="pt-5 md:w-4/5">
        <h3 className="text-lg font-semibold">Bình luận</h3>
        <div className="flex py-3">
          <i className="fa fa-user-circle w-16 h-16 text-6xl text-slate-300"></i>
          <div className="ml-5 md:w-full md:ml-10">
            <div className="flex">
              <h3 className="pr-3">Đánh giá:</h3>
              {renderRating()}
            </div>
            <TextArea
              onChange={handleUserComment}
              value={userComment}
              rows={5}
              cols={50}
              bordered
            ></TextArea>
          </div>
        </div>
        <div className="text-center md:text-right">
          <Button
            onClick={handleAddCommentButton}
            className="bg-rose-500 text-white"
          >
            Add comment
          </Button>
        </div>
      </div>
      <Modal
        title=""
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <LogInPage />
      </Modal>
    </div>
  );
}

export default CommentsByRoomID;
