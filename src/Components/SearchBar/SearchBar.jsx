import React, { useEffect, useState } from "react";
import { locationService } from "../../Services/location.service";
import { useNavigate } from "react-router";
import { DatePicker, Input, message, Space } from "antd";
import {
  setStartDateStore,
  setEndDateStore,
  setGuestsNumStore,
  setLocationStore,
} from "../../Redux/userSlice";
import { useDispatch } from "react-redux";

function SearchBar() {
  const { RangePicker } = DatePicker;
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const [locationList, setLocationList] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [userInput, setUserInput] = useState("");
  const [guestsNum, setGuestsNum] = useState("");
  const [selectedLocation, setSelectedLocation] = useState("");
  const fetchViTri = async () => {
    try {
      const res = await locationService.getAllLocations();
      // console.log(res.data.content);
      setLocationList(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const handleSelectDates = (dates) => {
    const [start, end] = dates;
    dispatch(setStartDateStore(JSON.stringify(start)));
    dispatch(setEndDateStore(JSON.stringify(end)));
  };
  const renderDateSelection = () => {
    return (
      <div className="border-l-2 flex flex-col px-3 w-1/3">
        <label className="font-medium">Chọn ngày</label>
        <div>
          <Space direction="vertical" size={12}>
            <RangePicker
              format={"DD/MM/YYYY"}
              onChange={(dates) => handleSelectDates(dates)}
            />
          </Space>
        </div>
      </div>
    );
  };
  const renderOptions = () => {
    return filteredData.map((location) => {
      return (
        <div
          key={location.id}
          onClick={() => {
            setUserInput(location.tenViTri);
            setSelectedLocation(location);
            setFilteredData([]);
          }}
          value={location.tenViTri}
          className="flex items-center px-5 pt-3"
        >
          <i className="fa fa-map-marker-alt bg-blue-300 rounded p-2"></i>
          <span className="pl-3 text-sm">
            {location.tenViTri}, {location.tinhThanh}, {location.quocGia}
          </span>
        </div>
      );
    });
  };
  const handleFilter = (e) => {
    let input = e.target.value;
    setUserInput(input);
    const newFilter = locationList.filter((location) => {
      return location.tenViTri.toLowerCase().includes(input.toLowerCase());
    });
    if (input === "") {
      setFilteredData("");
    } else {
      setFilteredData(newFilter);
    }
  };
  const handleSearchButton = () => {
    if (selectedLocation !== "") {
      dispatch(setLocationStore(selectedLocation));
      navigate(`/${selectedLocation.tinhThanh}`);
    } else {
      message.error("Vui lòng chọn địa điểm muốn đến!");
      return;
    }
  };
  useEffect(() => {
    fetchViTri();
  }, []);
  return (
    <div className="container mx-auto bg-white ">
      <form className="flex items-center w-full mx-auto justify-between bg-slate-50 shadow-md rounded-full p-3 md:w-4/5">
        <div className="relative w-1/3 px-3 flex flex-col">
          <label className="font-medium">Địa điểm</label>
          <Input
            allowClear
            type="text"
            className="outline-transparent"
            onChange={handleFilter}
            value={userInput}
            placeholder="Bạn sắp đi đâu?"
          ></Input>
        </div>
        {renderDateSelection()}
        <div className="border-l-2 w-1/6 flex flex-col pl-3">
          <label className="font-medium">Khách</label>
          <input
            type="number"
            className="outline-transparent"
            onChange={(e) => {
              setGuestsNum(e.target.value);
              dispatch(setGuestsNumStore(guestsNum));
            }}
            value={guestsNum}
            placeholder="Thêm khách"
          ></input>
        </div>
        <div
          onClick={() => handleSearchButton()}
          className="bg-rose-500 rounded-full p-4 text-white flex items-center justify-center"
        >
          <i className="fa fa-search"></i>
        </div>
      </form>
      {filteredData.length !== 0 && (
        <div className="flex flex-col justify-evenly m-3 z-50 w-full md:w-1/3 bg-slate-50 rounded-xl ">
          {renderOptions()}
        </div>
      )}
    </div>
  );
}

export default SearchBar;
