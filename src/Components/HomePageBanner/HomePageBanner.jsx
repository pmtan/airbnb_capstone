import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { setLocationStore } from "../../Redux/userSlice";
import { locationService } from "../../Services/location.service";

function HomePageBanner() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const [locations, setLocations] = useState([]);
  const handleClick = (location) => {
    dispatch(setLocationStore(location));
    navigate(`/${location.tinhThanh}`);
  };
  const fetchLocations = async () => {
    try {
      let res = await locationService.getAllLocations();
      // console.log(res.data.content);
      setLocations(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const renderBannerGrid = () => {
    return locations.map((viTri) => {
      return (
        <div
          onClick={() => handleClick(viTri)}
          key={viTri.id}
          className="flex flex-col items-center w-full"
        >
          <img
            src={viTri.hinhAnh}
            className="w-full md:object-cover md:w-60 md:h-60 rounded-lg"
            alt={viTri.tenViTri}
          ></img>
          <h5 className="pt-2">
            {viTri.tenViTri}, {viTri.tinhThanh}, {viTri.quocGia}
          </h5>
        </div>
      );
    });
  };
  useEffect(() => {
    fetchLocations();
  }, []);
  return (
    <div className="container mx-auto w-full grid md:grid-cols-6 gap-5 py-10">
      {renderBannerGrid()}
    </div>
  );
}

export default HomePageBanner;
