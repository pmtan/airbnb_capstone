import React, { useState } from "react";
import { useNavigate } from "react-router";
import LoginModal from "../LoginModal/LoginModal";

function HomePageNavBar() {
  let navigate = useNavigate();
  const [navbar, setNavbar] = useState(false);
  return (
    <nav className="container mx-auto rounded">
      <div className="justify-between p-2 mx-auto md:items-center md:flex ">
        <div>
          <div className="flex items-center justify-between py-1 md:py-5 md:block">
            <div onClick={() => navigate("/")} className="flex items-center">
              <i className="fab fa-airbnb text-rose-500 text-3xl " />
              <span className="text-rose-500 text-3xl font-semibold pl-2">
                airbnb
              </span>
            </div>
            <div className="md:hidden">
              <button
                className="p-2 text-slate-900 rounded-md outline-none focus:border-rose-500 focus:text-rose-500"
                onClick={() => setNavbar(!navbar)}
              >
                {navbar ? (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-6 h-6"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                ) : (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-6 h-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M4 6h16M4 12h16M4 18h16"
                    />
                  </svg>
                )}
              </button>
            </div>
          </div>
        </div>
        <div>
          <div
            className={`flex-1 justify-self-center shadow-lg md:shadow-none rounded-lg mb-3 pb-2 mt-3 md:block md:pb-0 md:mt-0 ${
              navbar ? "block" : "hidden"
            }`}
          >
            <ul className="items-center justify-center space-y-4 md:flex md:space-x-6 md:space-y-0">
              <li className="text-slate-900 focus:text-rose-500">
                <h3 className="px-3">Nơi ở</h3>
              </li>
              <li className="text-slate-900 focus:text-rose-500">
                <h3 className="px-3">Trải nghiệm</h3>
              </li>
              <li className="text-slate-900 focus:text-rose-500">
                <h3 className="px-3">Trải nghiệm trực tuyến</h3>
              </li>
              <li className="text-slate-900 focus:text-rose-500">
                <h3 className="px-3">Liên Hệ</h3>
              </li>
              <li>
                <div className="flex flex-col md:items-center  md:flex-row">
                  <h3 className="px-3">Đón tiếp khách</h3>
                  <h3 className="px-3">
                    <i className="fa fa-globe hidden md:block"></i>
                  </h3>
                </div>
              </li>
              <li>
                <LoginModal />
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default HomePageNavBar;
