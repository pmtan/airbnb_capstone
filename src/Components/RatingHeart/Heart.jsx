import React from "react";

function Heart({ marked, heartId }) {
  return (
    <span data-heart-id={heartId} className="heart text-sm" role="button">
      {marked ? "\u2764" : "\u2661"}
    </span>
  );
}

export default Heart;
