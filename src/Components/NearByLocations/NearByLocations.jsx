import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { setLocationStore } from "../../Redux/userSlice";
import { locationService } from "../../Services/location.service";

function NearByLocations() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const [nearByLocations, setNearByLocations] = useState([]);
  const renderNearByLocations = () => {
    return nearByLocations.map((city) => {
      return (
        <div
          onClick={() => handleClick(city)}
          key={city.id}
          className="flex flex-col items-center md:flex-row"
        >
          <img
            src={city.hinhAnh}
            alt={city.tinhThanh}
            className="w-20 h-20 rounded"
          />
          <div className="md:pl-5">
            <h3 className="font-medium">{city.tinhThanh}</h3>
            <p className=" text-slate-400">3 giờ lái xe</p>
          </div>
        </div>
      );
    });
  };

  const handleClick = (location) => {
    dispatch(setLocationStore(location));
    navigate(`/${location.tinhThanh}`);
  };
  const fetchNearByLocations = async () => {
    const params = {
      pageIndex: 1,
      pageSize: 8,
    };
    try {
      let res = await locationService.getNearByLocations(params);
      setNearByLocations(res.data.content.data);
      // console.log(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchNearByLocations();
  }, []);
  return (
    <div className="container mx-auto p-3">
      <h3 className="text-lg font-bold">Khám phá những điểm đến gần đây</h3>
      <div className="grid pt-5 grid-cols-3 md:grid-cols-4 md:gap-3">
        {renderNearByLocations()}
      </div>
    </div>
  );
}

export default NearByLocations;
