import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DatePicker, Input, Space } from "antd";
import {
  setStartDateStore,
  setEndDateStore,
  setGuestsNumStore,
  setLocationStore,
} from "../../Redux/userSlice";
import { useNavigate } from "react-router";
import { locationService } from "../../Services/location.service";
import dayjs from "dayjs";
import LoginModal from "../LoginModal/LoginModal";

function BookingNavBar() {
  const { RangePicker } = DatePicker;
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const { location, startDate, endDate, guestsNum } = useSelector(
    (state) => state.user
  );
  const [navbar, setNavbar] = useState(false);
  const [newGuestsNum, setNewGuestsNum] = useState("");
  const [userInput, setUserInput] = useState("");
  const [locationList, setLocationList] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [selectedLocation, setSelectedLocation] = useState(location);
  const fetchTatCaViTri = async () => {
    try {
      let res = await locationService.getAllLocations();
      // console.log(res.data.content);
      setLocationList(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const handleFilter = (e) => {
    let input = e.target.value;
    setUserInput(input);
    const newFilter = locationList.filter((lcatn) => {
      return lcatn.tenViTri.toLowerCase().includes(input.toLowerCase());
    });
    if (input === "") {
      setFilteredData("");
    } else {
      setFilteredData(newFilter);
    }
  };
  const handleSearchButton = () => {
    if (selectedLocation !== null) {
      dispatch(setLocationStore(selectedLocation));
      dispatch(setGuestsNumStore(newGuestsNum));
      navigate(`/${selectedLocation.tinhThanh}`);
    } else {
      alert("Vui lòng chọn địa điểm bạn muốn đến!");
      return;
    }
  };
  const handleSelectDates = (dates) => {
    const [start, end] = dates;
    dispatch(setStartDateStore(JSON.stringify(start)));
    dispatch(setEndDateStore(JSON.stringify(end)));
  };
  const renderOptions = () => {
    return filteredData.map((location) => {
      return (
        <div
          key={location.id}
          onClick={() => {
            setUserInput(location.tenViTri);
            setSelectedLocation(location);
            dispatch(setLocationStore(location));
            setFilteredData([]);
          }}
          value={location.tenViTri}
          className="flex items-center px-5 pt-3"
        >
          <i className="fa fa-map-marker-alt bg-blue-300 rounded p-2"></i>
          <span className="pl-3 text-sm">
            {location.tenViTri}, {location.tinhThanh}, {location.quocGia}
          </span>
        </div>
      );
    });
  };
  const renderDateSelection = () => {
    if (startDate == null || endDate == null) {
      return (
        <div className="border-l-2 flex flex-col px-3">
          <p>Chọn ngày</p>
          <div>
            <Space direction="vertical" size={12}>
              <RangePicker
                format={"DD/MM/YYYY"}
                onChange={(dates) => handleSelectDates(dates)}
              />
            </Space>
          </div>
        </div>
      );
    } else {
      return (
        <div className="flex w-full md:w-full">
          <div className="border-l-2 flex flex-col pl-1 md:px-6">
            <label className="text-xs md:text-lg">Nhận phòng</label>
            <div className="relative">
              <Space direction="vertical">
                <DatePicker
                  defaultValue={dayjs(JSON.parse(startDate))}
                  format={"DD/MM/YYYY"}
                  onChange={(date) =>
                    dispatch(setEndDateStore(JSON.stringify(date)))
                  }
                />
              </Space>
            </div>
          </div>
          <div className="border-l-2 pl-1 flex flex-col md:px-6">
            <label className="text-xs md:text-lg">Trả phòng</label>
            <div className="relative">
              <div className="flex md:absolute inset-y-0 left-0 items-center pl-3 pointer-events-none"></div>
              <Space direction="vertical">
                <DatePicker
                  defaultValue={dayjs(JSON.parse(startDate))}
                  format={"DD/MM/YYYY"}
                  disabledDate={(d) => !d || d.isBefore(JSON.parse(startDate))}
                  onChange={(date) =>
                    dispatch(setEndDateStore(JSON.stringify(date)))
                  }
                />
              </Space>
            </div>
          </div>
        </div>
      );
    }
  };
  useEffect(() => {
    fetchTatCaViTri();
  }, []);
  return (
    <div className="container mx-auto">
      <nav className="w-full mx-auto z-10 ">
        <div className="justify-between p-2 mx-auto md:items-center md:flex ">
          <div>
            <div className="flex items-center justify-between py-1 md:py-5 md:block">
              <div onClick={() => navigate("/")} className="flex items-center">
                <i className="fab fa-airbnb text-rose-500 text-3xl " />
                <span className="text-rose-500 text-3xl font-semibold pl-2">
                  airbnb
                </span>
              </div>
              <div className="md:hidden">
                <button
                  className="p-2 text-slate-900 rounded-md outline-none focus:border-rose-500 focus:text-rose-500"
                  onClick={() => setNavbar(!navbar)}
                >
                  {navbar ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="w-6 h-6"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fillRule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="w-6 h-6"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M4 6h16M4 12h16M4 18h16"
                      />
                    </svg>
                  )}
                </button>
              </div>
            </div>
          </div>
          <div>
            <div
              className={`flex-1 justify-self-center pb-3 mt-3 md:block md:pb-0 md:mt-0 ${
                navbar ? "block" : "hidden"
              }`}
            >
              <ul className="items-center justify-center space-y-4 md:flex md:space-x-6 md:space-y-0">
                <li>
                  <div className="flex flex-col md:items-center  md:flex-row">
                    <h3 className="px-3">Trở thành chủ nhà</h3>
                    <h3 className="px-3">
                      <i className="fa fa-globe hidden md:block"></i>
                    </h3>
                  </div>
                </li>
                <li>
                  <LoginModal />
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <div className="flex flex-col md:flex-row md:w-1/2 w-full mx-auto md:absolute md:top-3 md:translate-x-1/4">
        <form className="flex w-full items-center justify-between bg-white shadow-md rounded-lg md:rounded-full md:px-5 p-2">
          <div className="relative w-1/3 md:p-3 flex flex-col">
            <p>Nơi đến</p>
            <Input
              allowClear
              type="text"
              className="outline-transparent text-sm md:text-lg"
              onChange={handleFilter}
              value={userInput}
              placeholder={location ? location.tinhThanh : "Bạn sắp đi đâu?"}
            ></Input>
            {filteredData.length !== 0 && (
              <div className="flex flex-col w-screen md:w-full justify-evenly absolute top-10 m-3 z-50 bg-slate-50 rounded-xl ">
                {renderOptions()}
              </div>
            )}
          </div>
          <div className="w-1/2">{renderDateSelection()}</div>
          <div className="border-l-2 md:w-1/5 flex flex-col pl-2">
            <p>{guestsNum ? guestsNum : "0"} khách</p>
            <input
              type="number"
              className="outline-transparent text-xs md:text-lg w-full"
              onChange={(e) => {
                setNewGuestsNum(e.target.value);
                dispatch(setGuestsNumStore(e.target.value));
              }}
              value={newGuestsNum}
              placeholder="Thêm khách"
            ></input>
          </div>
          <div
            onClick={() => handleSearchButton()}
            className="bg-rose-500 rounded-full p-3 md:p-4 text-white flex items-center justify-center"
          >
            <i className="fa fa-search"></i>
          </div>
        </form>
      </div>
    </div>
  );
}

export default BookingNavBar;
