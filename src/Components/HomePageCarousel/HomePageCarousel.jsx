import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { setLocationStore } from "../../Redux/userSlice";
import { locationService } from "../../Services/location.service";
import { Carousel } from "antd";

function HomePageCarousel() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const [locations, setLocations] = useState([]);
  const handleClick = (location) => {
    dispatch(setLocationStore(location));
    navigate(`/${location.tinhThanh}`);
  };
  const fetchLocations = async () => {
    try {
      let res = await locationService.getAllLocations();
      // console.log(res.data.content);
      setLocations(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const renderCarouselContent = () => {
    return locations.map((viTri) => {
      return (
        <div
          onClick={() => handleClick(viTri)}
          key={viTri.id}
          className="flex flex-col items-center justify-center"
        >
          <img
            src={viTri.hinhAnh}
            className="w-full md:object-cover md:w-60 md:h-60"
            alt={viTri.tenViTri}
          ></img>
          <div className="pt-2 text-lg font-semibold">
            {viTri.tenViTri}, {viTri.tinhThanh}, {viTri.quocGia}
          </div>
        </div>
      );
    });
  };
  useEffect(() => {
    fetchLocations();
  }, []);
  return (
    <div className="pt-5">
      <Carousel autoplay dots>
        {renderCarouselContent()}
      </Carousel>
    </div>
  );
}

export default HomePageCarousel;
