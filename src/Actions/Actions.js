export function isBetween(x, min, max) {
  return x >= min && x <= max;
}
