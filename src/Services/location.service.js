import { https } from "./url.config";

export const locationService = {
  getAllLocations: () => {
    let uri = "/api/vi-tri";
    return https.get(uri);
  },
  getRoomsByLocation: (params) => {
    let uri = "/api/phong-thue/lay-phong-theo-vi-tri";
    return https.get(uri, { params });
  },
  getNearByLocations: (params) => {
    let uri = "/api/vi-tri/phan-trang-tim-kiem";
    return https.get(uri, { params });
  },
  getRoomDetail: (id) => {
    let uri = `/api/phong-thue/${id}`;
    return https.get(uri);
  },
  getAvailableBookingData: () => {
    let uri = "/api/dat-phong";
    return https.get(uri);
  },
};
