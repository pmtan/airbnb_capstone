import { https } from "./url.config";

export const commentService = {
  getCommentsByRoomID: (roomID) => {
    let uri = `/api/binh-luan/lay-binh-luan-theo-phong/${roomID}`;
    return https.get(uri);
  },
  postComment: (data) => {
    let uri = "/api/binh-luan";
    return https.post(uri, data);
  },
};
