import { https } from "./url.config";

export const userService = {
  logIn: (data) => {
    let uri = "/api/auth/signin";
    return https.post(uri, data);
  },
  signUp: (data) => {
    let uri = "/api/auth/signup";
    return https.post(uri, data);
  },
  getUserInfo: (userID) => {
    let uri = `/api/users/${userID}`;
    return https.get(uri);
  },
  updateUserInfo: (userID, newInfo) => {
    let uri = `/api/users/${userID}`;
    return https.put(uri, newInfo);
  },
  getBookingHistory: (MaNguoiDung) => {
    let uri = `/api/dat-phong/lay-theo-nguoi-dung/${MaNguoiDung}`;
    return https.get(uri);
  },
};
