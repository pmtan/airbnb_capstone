import axios from "axios";
import { store } from "..";
import { batLoading, tatLoading } from "../Redux/spinnerSlice";
import { localService } from "./local.service";

export const BASE_URL = "https://airbnbnew.cybersoft.edu.vn";
export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzM0UiLCJIZXRIYW5TdHJpbmciOiIyNi8wNC8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODI0NjcyMDAwMDAiLCJuYmYiOjE2NTQzNjIwMDAsImV4cCI6MTY4MjYxNDgwMH0.GGqFf8-ZXIqAjnaJZ40LjQvUHb1VyvRv3XtEIsMe_qE";
export const USER_TOKEN = localService.get()?.token;

export const https = axios.create({
  baseURL: BASE_URL,
  headers: {
    token: USER_TOKEN,
    tokenCybersoft: TOKEN_CYBERSOFT,
  },
});
// Add a request interceptor
https.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    // console.log("Before Axios Request");
    store.dispatch(batLoading());
    return config;
  },
  function (error) {
    // Do something with request error
    // console.log("Before Axios Request Error!");
    store.dispatch(tatLoading());
    return Promise.reject(error);
  }
);

// Add a response interceptor
https.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    // console.log("Axios Response");
    store.dispatch(tatLoading());
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    // console.log("Axios Response Error!");
    store.dispatch(tatLoading());
    return Promise.reject(error);
  }
);
