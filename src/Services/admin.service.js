import { https } from "./url.config";

export const adminService = {
  getBookingData: () => {
    let uri = "/api/dat-phong";
    return https.get(uri);
  },
  getRoomsInfoData: (params) => {
    let uri = "/api/phong-thue/phan-trang-tim-kiem";
    return https.get(uri, { params });
  },
  deleteRoom: (id) => {
    let uri = `/api/phong-thue/${id}`;
    return https.delete(uri);
  },
  getLocationsInfoData: (params) => {
    let uri = "/api/vi-tri/phan-trang-tim-kiem";
    return https.get(uri, { params });
  },
  uploadRoomPhoto: (data, params) => {
    let uri = "/api/phong-thue/upload-hinh-phong";
    return https.post(uri, data, { params });
  },
  uploadLocationPhoto: (data, params) => {
    let uri = "/api/vi-tri/upload-hinh-vitri";
    return https.post(uri, data, { params });
  },
  deleteLocation: (locationID) => {
    let uri = `/api/vi-tri/${locationID}`;
    return https.delete(uri);
  },
  deleteRoom: (roomID) => {
    let uri = `/api/phong-thue/${roomID}`;
    return https.delete(uri);
  },
  getUsersData: (params) => {
    let uri = "/api/users/phan-trang-tim-kiem";
    return https.get(uri, { params });
  },
};
