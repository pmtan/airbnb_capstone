export const LOCAL_INFO = "LOCAL_INFO";

export const localService = {
  set: (data) => {
    let jsonData = JSON.stringify(data);
    localStorage.setItem(LOCAL_INFO, jsonData);
  },
  get: () => {
    let data = localStorage.getItem(LOCAL_INFO);
    return JSON.parse(data);
  },
  remove: () => {
    localStorage.removeItem(LOCAL_INFO);
  },
};
