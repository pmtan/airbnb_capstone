import { https } from "./url.config";

export const bookingService = {
  confirmBooking: (data) => {
    let uri = "/api/dat-phong";
    return https.post(uri, data);
  },
};
