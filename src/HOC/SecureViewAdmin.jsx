import { message } from "antd";
import { useEffect } from "react";
import { localService } from "../Services/local.service";

export default function SecureViewAdmin({ Component }) {
  useEffect(() => {
    let { user } = localService.get();
    if (user == null || user.role !== "ADMIN") {
      window.location.href = "/";
      localService.remove();
    }
  }, []);
  return <Component />;
}
