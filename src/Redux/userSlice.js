import { createSlice } from "@reduxjs/toolkit";
import { localService } from "../Services/local.service";

const initialState = {
  userInfo: localService.get() ? localService.get() : null,
  location: null,
  startDate: null,
  endDate: null,
  guestsNum: null,
  roomID: null,
};
export const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfoStore: (state, action) => {
      state.userInfo = action.payload;
    },
    setLocationStore: (state, action) => {
      state.location = action.payload;
    },
    setStartDateStore: (state, action) => {
      state.startDate = action.payload;
    },
    setEndDateStore: (state, action) => {
      state.endDate = action.payload;
    },
    setGuestsNumStore: (state, action) => {
      state.guestsNum = action.payload;
    },
    setRoomID: (state, action) => {
      state.roomID = action.payload;
    },
  },
});

export const {
  setUserInfoStore,
  setLocationStore,
  setStartDateStore,
  setEndDateStore,
  setGuestsNumStore,
  setRoomID,
} = userSlice.actions;
export default userSlice.reducer;
