import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { locationService } from "../../Services/location.service";
import { DatePicker, message, Space, Modal, Button } from "antd";
import LogInPage from "../LogInPage/LogInPage";
import {
  setEndDateStore,
  setStartDateStore,
  setGuestsNumStore,
} from "../../Redux/userSlice";
import CommentsByRoomID from "../../Components/Comments/CommentsByRoomID";
import { bookingService } from "../../Services/booking.service";
import Footer from "../../Components/Footer/Footer";
import BookingNavBar from "../../Components/BookingNavBar/BookingNavBar";
import { useNavigate } from "react-router";
import dayjs from "dayjs";

function RoomDetail() {
  let { roomID, location, startDate, endDate, guestsNum, userInfo } =
    useSelector((state) => state.user);
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const { RangePicker } = DatePicker;
  const [roomDetail, setRoomDetail] = useState({});
  const [newGuestsNum, setNewGuestsNum] = useState("");
  const [bookingData, setBookingData] = useState([]);
  const fetchRoomDetail = async () => {
    try {
      let res = await locationService.getRoomDetail(roomID);
      // console.log(res.data.content);
      setRoomDetail(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const unavailableDates = [];
  for (let i = 0; i < bookingData.length; i++) {
    if (bookingData[i].maPhong === roomID) {
      let range = {
        start: new Date(bookingData[i].ngayDen),
        end: new Date(bookingData[i].ngayDi),
      };
      unavailableDates.push(range);
    }
  }
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const fetchAvailableDates = async () => {
    try {
      let res = await locationService.getAvailableBookingData();
      setBookingData(res.data.content);
      // console.log(bookingData);
    } catch (err) {
      console.log(err);
    }
  };
  const handleSelectDates = (dates) => {
    const [start, end] = dates;
    dispatch(setStartDateStore(JSON.stringify(start)));
    dispatch(setEndDateStore(JSON.stringify(end)));
  };

  const getTotalDays = () => {
    if (startDate !== "" && endDate !== "") {
      return dayjs(JSON.parse(endDate)).diff(
        dayjs(JSON.parse(startDate)),
        "days"
      );
    } else {
      message.error("Vui lòng chọn ngày đặt phòng!");
    }
  };
  const renderDateSelection = () => {
    if (startDate == null || endDate == null) {
      return (
        <div className="p-2">
          <label className="text-md font-medium">Chọn ngày</label>
          <div>
            <Space direction="vertical" size={12}>
              <RangePicker
                className="w-full"
                format={"DD/MM/YYYY"}
                onChange={(dates) => handleSelectDates(dates)}
              />
            </Space>
          </div>
        </div>
      );
    } else {
      return (
        <div className="flex w-full text-center">
          <div className="w-1/2">
            <label className="text-xs md:text-lg font-medium">Nhận phòng</label>
            <div>
              <Space direction="vertical">
                <DatePicker
                  defaultValue={dayjs(JSON.parse(startDate))}
                  format={"DD/MM/YYYY"}
                  onChange={(date) =>
                    dispatch(setEndDateStore(JSON.stringify(date)))
                  }
                />
              </Space>
            </div>
          </div>
          <div className="border-l-2 flex flex-col w-1/2 p-1">
            <label className="text-xs md:text-lg font-medium">Trả phòng</label>
            <div>
              <Space direction="vertical">
                <DatePicker
                  defaultValue={dayjs(JSON.parse(endDate))}
                  format={"DD/MM/YYYY"}
                  onChange={(date) =>
                    dispatch(setEndDateStore(JSON.stringify(date)))
                  }
                />
              </Space>
            </div>
          </div>
        </div>
      );
    }
  };
  const handleDatPhong = async () => {
    if (userInfo) {
      // console.log(userInfo);
      let data = {
        id: 0,
        maPhong: roomID,
        ngayDen: JSON.parse(startDate),
        ngayDi: JSON.parse(endDate),
        soLuongKhach: guestsNum,
        maNguoiDung: userInfo.id,
      };
      try {
        let res = await bookingService.confirmBooking(data);
        if (res.data.content) {
          message.success(res.data.message);
          navigate("/");
        }
      } catch (err) {
        console.log(err);
      }
    } else {
      message.error("Bạn chưa đăng nhập, đăng nhập để tiếp tục!");
      showModal();
    }
  };
  useEffect(() => {
    fetchRoomDetail();
    fetchAvailableDates();
  }, [roomID]);
  return (
    <div className="w-full">
      <BookingNavBar />
      <div className="container mx-auto">
        <div className="w-full mx-auto py-6">
          <h3 className="md:text-lg font-semibold">{roomDetail.tenPhong}</h3>
          <div className="flex flex-col md:flex-row justify-between pb-3">
            <div className="pr-3 grid grid-cols-2 md:grid-cols-3">
              <div className="flex items-center">
                <span className="w-1 h-1 bg-gray-500 rounded-full mx-1"></span>

                <i className="fa fa-star text-rose-500"></i>
                <p className="ml-2 text-sm font-bold text-slate-500">4.95</p>
                <span
                  href="#"
                  className="text-sm font-medium text-slate-500 underline hover:no-underline hover:text-rose-500 px-2"
                >
                  (73 reviews)
                </span>
              </div>
              <div className="flex items-center">
                <span className="w-1 h-1 bg-gray-500 rounded-full mx-1"></span>
                <div className="flex items-center justify-center">
                  <i className="fa fa-heart text-rose-500"></i>
                </div>
                <span className="text-sm font-medium text-slate-500 px-1">
                  Chủ nhà siêu cấp
                </span>
              </div>
              <div className="flex items-center">
                <span className="w-1 h-1 bg-gray-500 rounded-full mx-1"></span>
                <span
                  href="#"
                  className="text-sm font-medium text-slate-500 underline hover:no-underline hover:text-rose-500"
                >
                  {location.tenViTri}, {location.tinhThanh},{location.quocGia}
                </span>
              </div>
            </div>
            <div className="text-right">
              <i className="fa fa-upload hover:text-rose-500"></i>
              <span
                href="#"
                className="text-sm font-medium text-slate-900 underline hover:no-underline hover:text-rose-500 px-2"
              >
                Chia sẻ
              </span>
              <i className="fa fa-heart text-slate-400 hover:text-rose-500"></i>
              <span
                href="#"
                className="text-sm font-medium text-slate-900 underline hover:no-underline hover:text-red-500 px-2"
              >
                Lưu
              </span>
            </div>
          </div>
          <img src={roomDetail.hinhAnh} className="w-full" alt=""></img>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-3">
          <div className="col-span-2 md:mr-20 px-3">
            <p className="text-lg font-semibold">Toàn bộ căn hộ, condo</p>
            <div className="flex items-center border-b-2 pb-3">
              <span className="w-1 h-1 bg-gray-500 rounded-full mx-1"></span>
              <div className="text-slate-500">{roomDetail.khach} khách</div>
              <span className="w-1 h-1 bg-gray-500 rounded-full mx-1"></span>
              <div className="text-slate-500">
                {roomDetail.phongNgu} phòng ngủ
              </div>
              <span className="w-1 h-1 bg-gray-500 rounded-full mx-1"></span>
              <div className="text-slate-500">{roomDetail.giuong} giường</div>
              <span className="w-1 h-1 bg-gray-500 rounded-full mx-1"></span>
              <div className="text-slate-500">
                {roomDetail.phongTam} phòng tắm
              </div>
            </div>
            <div className="border-b-2 mb-5">
              <div className="flex py-2">
                <i className="fa fa-home text-xl pr-3"></i>
                <div className="flex flex-col">
                  <h3>Toàn bộ nhà</h3>
                  <p>Bạn sẽ có chung cư cao cấp cho riêng mình.</p>
                </div>
              </div>
              <div className="flex py-2">
                <i className="fa fa-leaf text-xl pr-3"></i>
                <div className="flex flex-col">
                  <h3>Vệ sinh tăng cường</h3>
                  <p>
                    Chủ nhà này đã cam kết thực hiện quy trình vệ sinh tăng
                    cường 5 bước của Airbnb.
                  </p>
                  <span className="text-sm font-medium">Tìm hiểu thêm</span>
                </div>
              </div>
              <div className="flex py-2">
                <i className="fa fa-medal text-xl pr-3"></i>
                <div className="flex flex-col">
                  <h3>Chủ nhà siêu cấp</h3>
                  <p>
                    Chủ nhà siêu cấp là những chủ nhà có kinh nghiệm, được đánh
                    giá cao và là những người cam kết mang lại quãng thời gian ở
                    tuyệt vời cho khách.
                  </p>
                </div>
              </div>
              <div className="flex py-2">
                <i className="fa fa-calendar-alt text-xl pr-3"></i>
                <div className="flex flex-col">
                  <h3>Miễn phí huỷ trong 48 giờ</h3>
                </div>
              </div>
            </div>
            <div className="border border-slate-900 rounded flex justify-between p-2">
              <p>Dịch sang Tiếng Việt</p>
              <i className="fa fa-language"></i>
            </div>
            <div className="p-3">
              <p>{roomDetail.moTa}</p>
              <span className="underline text-sm">Hiện thị thêm</span>
              <i className="fa fa-angle-right"></i>
            </div>
            <div className="px-3">
              <h3 className="text-lg font-semibold ">Tiện nghi</h3>
              <div className="grid grid-cols-2">
                {roomDetail.bep ? (
                  <span>
                    <i className="fa fa-utensils text-xl p-2"></i> Bếp
                  </span>
                ) : (
                  ""
                )}
                {roomDetail.tivi ? (
                  <span>
                    <i className="fa fa-tv text-xl p-2"></i>TV truyền hình cáp
                    tiêu chuẩn
                  </span>
                ) : (
                  ""
                )}
                {roomDetail.dieuHoa ? (
                  <span>
                    <i className="fa fa-snowflake text-xl p-2"></i>Điều hoà
                    nhiệt độ
                  </span>
                ) : (
                  ""
                )}
                {roomDetail.loSuoi ? (
                  <span>
                    <i className="fa fa-temperature-high text-xl p-2"></i>Lò
                    sưởi trong nhà
                  </span>
                ) : (
                  ""
                )}
                {roomDetail.doXe ? (
                  <span>
                    <i className="fa fa-parking text-xl p-2"></i>Bãi đỗ xe
                  </span>
                ) : (
                  ""
                )}
                {roomDetail.wifi ? (
                  <span>
                    <i className="fa fa-wifi text-xl p-2"></i>Wifi
                  </span>
                ) : (
                  ""
                )}
                {roomDetail.thangMay ? (
                  <span>
                    <i className="fa fa-caret-square-up text-xl p-2"></i>Thang
                    máy
                  </span>
                ) : (
                  ""
                )}
                {roomDetail.banCong ? (
                  <span>
                    <i className="fa fa-city text-xl p-2"></i> Sân hoặc ban công
                  </span>
                ) : (
                  ""
                )}
                {roomDetail.tuLanh ? (
                  <span>
                    <i className="fa fa-door-closed text-xl p-2"></i>Tủ lạnh
                  </span>
                ) : (
                  ""
                )}
                {roomDetail.mayGiat ? (
                  <span>
                    <i className="fa fa-tshirt text-xl p-2"></i> Máy giặt
                  </span>
                ) : (
                  ""
                )}
                {roomDetail.hoBoi ? (
                  <span>
                    <i className="fa fa-swimming-pool text-xl p-2"></i>Hồ bơi
                  </span>
                ) : (
                  ""
                )}
                <span>
                  <i className="fa fa-calendar-check text-xl p-2"></i>Cho phép ở
                  dài hạn
                </span>
              </div>
            </div>
          </div>
          <div className="col-span-1 shadow-2xl flex flex-col rounded-md p-5 h-fit">
            <div className="w-full flex justify-between py-5">
              <div>&#36;{roomDetail.giaTien}/ đêm</div>
              <div className="">
                <i className="fa fa-star text-rose-500"></i>
                <span className="ml-2 text-sm font-bold text-slate-500">
                  4.95
                </span>
                <span
                  href="#"
                  className="text-sm font-medium text-slate-500 underline hover:no-underline hover:text-rose-500 px-2"
                >
                  (73 reviews)
                </span>
              </div>
            </div>
            <div className="flex border border-slate-400 rounded-t-md">
              {renderDateSelection()}
            </div>
            <div className="flex border border-slate-400 border-t-0 rounded-b-md w-full flex-col p-3">
              <label className="font-medium">Khách</label>
              <input
                type="number"
                className="outline-transparent"
                onChange={(e) => {
                  setNewGuestsNum(e.target.value);
                  dispatch(setGuestsNumStore(e.target.value));
                }}
                value={newGuestsNum}
                placeholder={guestsNum ? `${guestsNum} khách` : "Thêm khách"}
              ></input>
            </div>
            <Button
              size="large"
              onClick={handleDatPhong}
              className="w-full rounded-md bg-rose-500 text-lg text-white my-3"
            >
              Đặt phòng
            </Button>
            <p className=" text-slate-500 text-center">
              Bạn vẫn chưa bị trừ tiền
            </p>
            {startDate !== null && endDate !== null && (
              <div className="flex flex-col">
                <div className="flex justify-between p-2">
                  <p className="underline">
                    &#36;{roomDetail.giaTien} x {getTotalDays()}đêm
                  </p>
                  <span>
                    {" "}
                    &#36;{(roomDetail.giaTien * getTotalDays()).toFixed(2)}
                  </span>
                </div>
                <div className="flex justify-between border-b-2 p-2 pb-5">
                  <p className="underline">Phí dịch vụ</p>
                  <span>
                    {" "}
                    &#36;
                    {(roomDetail.giaTien * getTotalDays() * 0.1).toFixed(2)}
                  </span>
                </div>
                <div className="flex justify-between p-2 pb-5 text-lg font-semibold">
                  <p>Tổng</p>
                  <span>
                    {" "}
                    &#36;
                    {(
                      roomDetail.giaTien * getTotalDays() +
                      roomDetail.giaTien * getTotalDays() * 0.1
                    ).toFixed(2)}
                  </span>
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="px-3">
          <CommentsByRoomID />
        </div>
        <Footer />
        <Modal
          footer={null}
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          className="w-full"
        >
          <LogInPage />
        </Modal>
      </div>
    </div>
  );
}

export default RoomDetail;
