import React, { useEffect, useState } from "react";
import { adminService } from "../../Services/admin.service";
import { Space, Table, Button, Modal, message } from "antd";

function RoomInfoManagement() {
  const [roomsInfo, setRoomsInfo] = useState([]);
  const [roomID, setRoomID] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const fetchRoomsInfo = async () => {
    let params = {
      pageIndex: 1,
      pageSize: 100,
    };
    try {
      let res = await adminService.getRoomsInfoData(params);
      setRoomsInfo(res.data.content.data);
    } catch (err) {
      console.log(err);
    }
  };
  const [selectedFile, setSelectedFile] = useState(null);
  const handleFileSelection = (e) => {
    setSelectedFile(e.target.files[0]);
  };
  const handleSubmitFile = async (e) => {
    e.preventDefault();
    let formData = new FormData();
    formData.append("formFile", selectedFile);
    let params = {
      maPhong: roomID,
    };
    try {
      let res = await adminService.uploadRoomPhoto(formData, params);
      fetchRoomsInfo();
      message.success("Cập nhật hình ảnh thành công!");
      // console.log(res);
    } catch (err) {
      // console.log(err.response.data.content);
      message.error(err.response.data.content);
    }
  };
  const showModal = (id) => {
    setRoomID(String(id));
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const handleDeleteRoom = async (id) => {
    try {
      let res = await adminService.deleteRoom(id);
      console.log(res.data);
      message.success(res.data.message);
      fetchRoomsInfo();
    } catch (err) {
      console.log(err);
    }
  };
  const columns = [
    {
      title: "Mã phòng",
      dataIndex: "id",
      key: "id",
      render: (text) => <span>{text}</span>,
    },
    {
      title: "Tên phòng",
      dataIndex: "tenPhong",
      key: "tenPhong",
    },
    {
      title: "Hình ảnh",
      dataIndex: "hinhAnh",
      key: "hinhAnh",
      render: (_, room) => {
        return (
          <div className="flex flex-col items-center">
            <img src={room.hinhAnh} className="w-20 h-20 rounded" alt=""></img>
            <Button
              onClick={() => showModal(room.id)}
              className="text-blue-500 border border-blue-500 m-2"
            >
              Chỉnh sửa
            </Button>
          </div>
        );
      },
    },

    {
      title: "Số khách tối đa",
      dataIndex: "khach",
      key: "khach",
    },

    {
      title: "Action",
      key: "action",
      render: (_, room) => (
        <Space size="middle">
          <Button className="bg-blue-500  rounded">Thông tin chi tiết</Button>
          <Button className="bg-yellow-500  rounded">Sửa</Button>
          <Button
            onClick={() => handleDeleteRoom(room.id)}
            className="bg-red-500  rounded"
          >
            Xoá
          </Button>
        </Space>
      ),
    },
  ];
  useEffect(() => {
    fetchRoomsInfo();
  }, []);
  return (
    <div>
      <Table columns={columns} dataSource={roomsInfo} className="w-full" />
      <Modal
        title="Cập nhật hình ảnh"
        footer={null}
        open={isModalOpen}
        onCancel={handleCancel}
      >
        <form onSubmit={handleSubmitFile}>
          <input
            className="border"
            type="file"
            onChange={handleFileSelection}
          />
          <Button
            onClick={handleSubmitFile}
            className="text-rose-500 border-rose-500"
            type="button"
          >
            Upload File
          </Button>
        </form>
      </Modal>
    </div>
  );
}

export default RoomInfoManagement;
