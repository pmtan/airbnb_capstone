import React, { useEffect, useState } from "react";
import { adminService } from "../../Services/admin.service";
import { Space, Table, Button, Modal } from "antd";
import dayjs from "dayjs";

function BookingsManagement() {
  const [bookings, setBookings] = useState([]);
  const fetchBookingData = async () => {
    try {
      let res = await adminService.getBookingData();
      //   console.log(res.data.content);
      setBookings(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const columns = [
    {
      title: "Booking ID",
      dataIndex: "id",
      key: "id",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Mã người dùng",
      dataIndex: "maNguoiDung",
      key: "maNguoDung",
    },
    {
      title: "Mã phòng",
      dataIndex: "maPhong",
      key: "maPhong",
    },
    {
      title: "Ngày đến",
      dataIndex: "ngayDen",
      key: "ngayDen",
      render: (_, booking) => {
        return <p>{dayjs(booking.ngayDen).format("DD/MM/YYYY")}</p>;
      },
    },
    {
      title: "Ngày đi",
      dataIndex: "ngayDi",
      key: "ngayDi",
      render: (_, booking) => {
        return <p>{dayjs(booking.ngayDi).format("DD/MM/YYYY")}</p>;
      },
    },
    {
      title: "Số lượng khách",
      key: "soLuongKhach",
      dataIndex: "soLuongKhach",
    },
  ];

  useEffect(() => {
    fetchBookingData();
  }, []);
  return <Table columns={columns} dataSource={bookings} className="w-full" />;
}

export default BookingsManagement;
