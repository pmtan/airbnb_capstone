import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { setUserInfoStore } from "../../Redux/userSlice";
import { localService } from "../../Services/local.service";
import UsersManagement from "./ UsersManagement";
import BookingsManagement from "./BookingsManagement";
import LocationManagement from "./LocationManagement";
import RoomInfoManagement from "./RoomInfoManagement";
import { UnorderedListOutlined } from "@ant-design/icons";
import { Button, Dropdown, message, Space, Tabs } from "antd";

function AdminPage() {
  let userInfo = localService.get();
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const handleDangXuat = () => {
    localService.remove();
    dispatch(setUserInfoStore(null));
    navigate("/");
  };
  const items = [
    {
      key: "0",
      label: (
        <h3 className="text-rose-500 text-xl font-medium">
          {userInfo.user.name}
        </h3>
      ),
    },
    {
      key: "1",
      label: (
        <span onClick={navigate("/user-info")} className="hover:text-rose-500">
          Thông tin cá nhân
        </span>
      ),
    },
    {
      key: "2",
      label: <Button onClick={handleDangXuat}>Đăng xuất</Button>,
    },
  ];
  const renderModal = () => {
    return (
      <Dropdown
        menu={{
          items,
        }}
      >
        <a onClick={(e) => e.preventDefault()}>
          <Space>
            <UnorderedListOutlined className="text-2xl hidden md:block" />
          </Space>
        </a>
      </Dropdown>
    );
  };
  return (
    <div className="container mx-auto flex flex-col">
      <div className="flex items-center justify-between py-5 relative w-full">
        <div onClick={() => navigate("/")} className="flex items-center">
          <i className="fab fa-airbnb text-rose-500 text-3xl " />
          <span className="text-rose-500 text-3xl font-semibold pl-2">
            airbnb
          </span>
        </div>
        <div className="flex items-center border-2 rounded-full px-2 py-1">
          <img
            src={userInfo.user.avatar}
            alt=""
            className="w-12 h-12 rounded-full"
          />
          {renderModal()}
        </div>
      </div>
      <div>
        <Space
          style={{
            marginBottom: 24,
          }}
        ></Space>
        <Tabs
          tabPosition="left"
          items={[
            {
              label: "Quản lý người dùng",
              key: "1",
              children: <UsersManagement />,
            },
            {
              label: "Quản lý thông tin vị trí",
              key: "2",
              children: <LocationManagement />,
            },
            {
              label: "Quản lý thông tin phòng",
              key: "3",
              children: <RoomInfoManagement />,
            },
            {
              label: "Quản lý đặt phòng",
              key: "4",
              children: <BookingsManagement />,
            },
          ]}
        />
      </div>
    </div>
  );
}

export default AdminPage;
