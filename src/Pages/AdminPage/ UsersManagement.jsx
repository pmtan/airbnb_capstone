import React, { useEffect, useState } from "react";
import { adminService } from "../../Services/admin.service";
import { Space, Table, Tag } from "antd";

function UsersManagement() {
  const [users, setUsers] = useState([]);
  const fetchBookingData = async () => {
    let params = {
      pageIndex: 1,
      pageSize: 100,
    };
    try {
      let res = await adminService.getUsersData(params);
      // console.log(res.data.content);
      setUsers(res.data.content.data);
    } catch (err) {
      console.log(err);
    }
  };
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Tên người dùng",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Password",
      dataIndex: "password",
      key: "password",
    },
    {
      title: "Phone",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "Vai trò",
      key: "role",
      dataIndex: "role",
      render: (_, user) => {
        let color = user.role === "USER" ? "geekblue" : "volcano";
        return <Tag color={color}>{user.role}</Tag>;
      },
    },
    {
      title: "Action",
      key: "action",
      render: () => (
        <Space size="middle">
          <button className="bg-yellow-500 p-2 m-2 rounded">Sửa</button>
          <button className="bg-red-500 p-2 m-2 rounded">Xoá</button>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    fetchBookingData();
  }, []);
  return <Table columns={columns} dataSource={users} className="w-full" />;
}

export default UsersManagement;
