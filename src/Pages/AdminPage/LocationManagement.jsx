import React, { useEffect, useState } from "react";
import { adminService } from "../../Services/admin.service";
import { Button, Space, Table, Modal, message } from "antd";

function LocationManagement() {
  const [locations, setLocations] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [locationID, setLocationID] = useState("");
  const showModal = (id) => {
    setLocationID(String(id));
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const fetchLocationsData = async () => {
    let params = {
      pageIndex: 1,
      pageSize: 100,
    };
    try {
      let res = await adminService.getLocationsInfoData(params);
      // console.log(res.data.content);
      handleOk();
      setLocations(res.data.content.data);
    } catch (err) {
      console.log(err);
    }
  };
  const columns = [
    {
      title: "Mã vị trí",
      dataIndex: "id",
      key: "id",
      render: (text) => <span>{text}</span>,
    },
    {
      title: "Tên vị trí",
      dataIndex: "tenViTri",
      key: "tenViTri",
    },
    {
      title: "Tỉnh thành",
      dataIndex: "tinhThanh",
      key: "tinhThanh",
    },
    {
      title: "Quốc gia",
      dataIndex: "quocGia",
      key: "quocGia",
    },
    {
      title: "Hình ảnh",
      dataIndex: "hinhAnh",
      key: "hinhAnh",
      render: (_, location) => {
        return (
          <div className="flex flex-col items-center">
            <img
              src={location.hinhAnh}
              className="w-20 h-20 rounded"
              alt=""
            ></img>
            <Button
              onClick={() => showModal(location.id)}
              className="text-blue-500 border border-blue-500 m-2"
            >
              Chỉnh sửa
            </Button>
          </div>
        );
      },
    },
    {
      title: "Action",
      key: "action",
      render: (_, location) => (
        <Space size="middle">
          <button className="bg-yellow-500 p-2 m-2 rounded">Sửa</button>
          <button
            onClick={() => handleDeleteLocation(location.id)}
            className="bg-red-500 p-2 m-2 rounded"
          >
            Xoá
          </button>
        </Space>
      ),
    },
  ];

  const [selectedFile, setSelectedFile] = useState(null);
  const handleFileSelection = (e) => {
    setSelectedFile(e.target.files[0]);
  };
  const handleSubmitFile = async (e) => {
    e.preventDefault();
    let formData = new FormData();
    formData.append("formFile", selectedFile);
    let params = {
      maViTri: locationID,
    };
    try {
      let res = await adminService.uploadLocationPhoto(formData, params);
      console.log(res.data);
      message.success("Cập nhật hình ảnh thành công");
      fetchLocationsData();
    } catch (err) {
      console.log(err);
      message.error("Đã có lỗi xảy ra!");
    }
  };
  const handleDeleteLocation = async (locationID) => {
    try {
      let res = await adminService.deleteLocation(locationID);
      console.log(res.data);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchLocationsData();
  }, []);
  return (
    <div>
      <Table
        columns={columns}
        dataSource={locations}
        className="w-full"
      ></Table>
      <Modal
        title="Cập nhật hình ảnh"
        footer={null}
        open={isModalOpen}
        onCancel={handleCancel}
      >
        <form onSubmit={handleSubmitFile}>
          <input
            className="border"
            type="file"
            onChange={handleFileSelection}
          />
          <Button
            onClick={handleSubmitFile}
            className="text-rose-500 border-rose-500"
            type="button"
          >
            Upload File
          </Button>
        </form>
      </Modal>
    </div>
  );
}

export default LocationManagement;
