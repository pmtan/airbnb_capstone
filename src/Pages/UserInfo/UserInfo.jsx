import React, { useEffect, useState } from "react";
import HomePageNavBar from "../../Components/HomePageNavBar/HomePageNavBar";
import { localService } from "../../Services/local.service";
import { userService } from "../../Services/user.service";
import dayjs from "dayjs";

function UserInfo() {
  const [userInfo, setUserInfo] = useState("");
  const [bookingHistory, setBookingHistory] = useState("");
  const id = localService.get()?.user.id;
  const fetchUserInfo = async () => {
    try {
      let res = await userService.getUserInfo(id);
      // console.log(res.data.content);
      setUserInfo(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const renderUserInfo = () => {
    return (
      <div className="border rounded-lg flex flex-col p-5 h-fit">
        <div className="flex flex-col items-center">
          {userInfo.avatar ? (
            <img src={userInfo.avatar} alt=""></img>
          ) : (
            <i className="fa fa-user-circle text-8xl text-slate-300"></i>
          )}
          <p className="text-slate-500 underline py-3">Cập nhật ảnh</p>
        </div>
        <div className="flex flex-col py-5">
          <div className="border-b-2 py-5">
            <i className="fa fa-user-check"></i>
            <h3 className="text-2xl font font-semibold py-2">
              Xác minh danh tính
            </h3>
            <p className="text-slate-600 py-2">
              Xác thực danh tính của bạn với huy hiệu xác minh danh tính
            </p>
            <button className="border-2 rounded bg-transparent text-slate-900 w-1/2 p-2 hover:bg-rose-500 hover:text-white ease-in duration-200">
              Nhận huy hiệu
            </button>
          </div>
          <div className="py-5">
            <h3 className="text-xl font-semibold">
              {userInfo.name} đã xác nhận
            </h3>
            <p className="py-2">
              <i className="fa fa-check pr-2"></i>
              Địa chỉ email
            </p>
          </div>
        </div>
      </div>
    );
  };
  const fetchBookingHistory = async () => {
    try {
      let res = await userService.getBookingHistory(id);
      // console.log(res.data.content);
      setBookingHistory(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  // <form action="">
  //       <label htmlFor="name">Họ tên</label>
  //       <input type="text" />
  //       <label htmlFor="email">Email</label>
  //       <input type="text" />
  //       <label htmlFor="password">Mật khẩu</label>
  //       <input type="text" />
  //       <label htmlFor="phone">Số ĐT</label>
  //       <input type="text" />
  //       <label htmlFor="birthday">Ngày sinh</label>
  //       <input type="text" />
  //       <label htmlFor="avatar">Avatar</label>
  //       <input type="text" />
  //     </form>
  const renderBookingHistory = () => {
    return bookingHistory.map((booking) => {
      return (
        <div key={booking.id} className="w-full border-b-2 py-3">
          <div>Booking ID: {booking.id}</div>
          <div>Mã phòng: {booking.maPhong}</div>
          <div>Ngày đến: {dayjs(booking.ngayDen).format("DD/MM/YYYY")}</div>
          <div>Ngày đi: {dayjs(booking.ngayDi).format("DD/MM/YYYY")}</div>
          <div>Số lượng khách: {booking.soLuongKhach}</div>
        </div>
      );
    });
  };
  useEffect(() => {
    fetchUserInfo();
    fetchBookingHistory();
  }, []);
  return (
    <div>
      <HomePageNavBar />
      <div className="container mx-auto flex">
        <div className="flex w-1/4 p-5">{renderUserInfo()}</div>
        <div className="flex flex-col p-5 w-3/4">
          <h3 className="text-3xl font-medium py-2">
            Xin chào, tôi là {userInfo.name}
          </h3>
          <p className="text-slate-500 py-1">Bắt đầu tham gia vào 2021.</p>
          <p className="text-sm underline font-medium">Chỉnh sửa hồ sơ</p>
          <div>
            <h1 className="text-3xl font-semibold py-5">Phòng đã thuê</h1>
            {bookingHistory.length > 0 && renderBookingHistory()}
          </div>
        </div>
      </div>
    </div>
  );
}

export default UserInfo;
