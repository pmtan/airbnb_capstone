import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { isBetween } from "../../Actions/Actions";
import BookingNavBar from "../../Components/BookingNavBar/BookingNavBar";
import Footer from "../../Components/Footer/Footer";
import { setRoomID } from "../../Redux/userSlice";
import { locationService } from "../../Services/location.service";

function RoomsAtLocation() {
  let { location, startDate, endDate } = useSelector((state) => state.user);
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const [roomsData, setRoomsData] = useState([]);
  const fetchRoomsAtLocation = async () => {
    let params = {
      maViTri: location.id,
    };
    try {
      let res = await locationService.getRoomsByLocation(params);
      setRoomsData(res.data.content);
      // console.log(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };

  const [bookingData, setBookingData] = useState([]);
  const fetchAvailableBookingData = async () => {
    try {
      let res = await locationService.getAvailableBookingData();
      // console.log(res.data.content);
      setBookingData(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const getUnavailableRooms = () => {
    const unavailableRooms = [];
    const startD = dayjs(JSON.parse(startDate));
    const endD = dayjs(JSON.parse(endDate));
    for (let i = 0; i < bookingData.length; i++) {
      const start = dayjs(bookingData[i].ngayDen);
      const end = dayjs(bookingData[i].ngayDi);
      if (isBetween(startD, start, end) || isBetween(endD, start, end)) {
        // console.log(bookingData[i]);
        unavailableRooms.push(bookingData[i].maPhong);
      }
    }
    return unavailableRooms;
  };

  const uniqueUnavailbleRooms = [...new Set(getUnavailableRooms())];
  // console.log(uniqueUnavailbleRooms);
  const availableRooms = roomsData.filter(
    (item) => !uniqueUnavailbleRooms.includes(item.id)
  );
  const handleSelectRoom = (roomID) => {
    dispatch(setRoomID(roomID));
    navigate(`/room/${roomID}`);
  };
  const renderRoomOptions = () => {
    // console.log(availableRooms);
    return availableRooms.map((room) => {
      return (
        <div
          onClick={() => handleSelectRoom(room.id)}
          key={room.id}
          className="w-full flex flex-col items-center p-3 md:p-0 md:flex-row border-b-2"
        >
          <div className="md:w-2/5 w-full">
            <img src={room.hinhAnh} className="w-full rounded-lg" alt=""></img>
          </div>
          <div className="md:w-3/5 md:p-5 flex flex-col">
            <p className="text-slate-500 pt-1">
              Toàn bộ căn hộ dịch vụ tại {location.tinhThanh}
            </p>
            <h3 className="md:text-xl font-medium pt-2">{room.tenPhong}</h3>
            <div className="grid grid-cols-2 md:grid-cols-5 pt-2">
              <div>
                <i className="fa fa-users"></i> {room.khach} khách
              </div>
              <div>
                <i className="fa fa-door-closed"></i> {room.phongNgu} phòng ngủ
              </div>
              <div>
                <i className="fa fa-bath"></i> {room.phongTam} phòng tắm
              </div>
              <div>
                <i className="fa fa-bed"></i> {room.giuong} giường
              </div>
            </div>
            <div className="text-right">
              <h3 className="text-xl"> &#36;{room.giaTien}/ đêm</h3>
            </div>
          </div>
        </div>
      );
    });
  };
  useEffect(() => {
    fetchRoomsAtLocation();
    fetchAvailableBookingData();
  }, [location.id]);
  return (
    <div>
      <BookingNavBar />
      <div className="container mx-auto flex flex-col md:flex-row pt-5">
        <div className="md:w-3/5 md:p-5">
          {availableRooms.length > 0 ? (
            renderRoomOptions()
          ) : (
            <div className="text-center text-rose-500">
              Rất tiếc, chưa có phòng theo tiêu chí của bạn.
            </div>
          )}
        </div>
        <div className="h-96 md:w-2/5 md:h-screen md:pr-5">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d37456.75912847742!2d106.68810182141003!3d10.798901208270236!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529292e8d3dd1%3A0xf15f5aad773c112b!2zSOG7kyBDaMOtIE1pbmgsIFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1672176537571!5m2!1svi!2s"
            width="100%"
            height="100%"
            style={{ border: 0 }}
            allowFullScreen
            loading="lazy"
            referrerPolicy="no-referrer-when-downgrade"
            title="map"
          />
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default RoomsAtLocation;
