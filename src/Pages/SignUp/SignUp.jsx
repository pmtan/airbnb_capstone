import { message } from "antd";
import React, { useState } from "react";
import { useNavigate } from "react-router";
import HomePageNavBar from "../../Components/HomePageNavBar/HomePageNavBar";
import { userService } from "../../Services/user.service";

function SignUp() {
  let navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState("");
  const onEmailChange = (e) => {
    setEmail(e.target.value);
  };
  const validateEmail = () => {
    let isValid = String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
    if (isValid) {
      setEmailError("");
    } else {
      setEmailError("Email không hợp lệ.");
    }
  };
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const onPasswordChange = (e) => {
    setPassword(e.target.value);
  };
  const validatePassword = () => {
    let isValid = String(password)
      .toLowerCase()
      .match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/);
    if (isValid) {
      setPasswordError("");
    } else {
      setPasswordError(
        "Password không hợp lệ.(Ít nhất 6 kí tự, ít nhất 1 chữ số và 1 chữ cái)"
      );
    }
  };

  const [confirmPassword, setConfirmPassword] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState("");
  const onConfirmPasswordChange = (e) => {
    let input = e.target.value;
    setConfirmPassword(input);
    validateConfirmPassword(input);
  };
  const validateConfirmPassword = (value) => {
    if (password === confirmPassword) {
      setConfirmPasswordError("");
    } else {
      setConfirmPasswordError("Password không trùng khớp.");
    }
  };

  const [name, setName] = useState("");
  const [nameError, setNameError] = useState("");
  const onNameChange = (e) => {
    setName(e.target.value);
  };
  const validateName = () => {
    if (name.length !== 0) {
      setNameError("");
    } else {
      setNameError("Tên không hợp lệ.");
    }
  };

  const [phone, setPhone] = useState("");
  const [phoneError, setPhoneError] = useState("");
  const onPhoneChange = (e) => {
    setPhone(e.target.value);
  };
  const validatePhone = () => {
    let isValid = phone.match(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g);
    if (isValid) {
      setPhoneError("");
    } else {
      setPhoneError("Số điện thoại không hợp lệ.");
    }
  };
  const [gender, setGender] = useState("");
  const [genderError, setGenderError] = useState("");
  const onGenderSelect = (e) => {
    setGender(e.target.value);
  };
  const validateGender = () => {
    if (gender) {
      setGenderError("");
    } else {
      setGenderError("Vui lòng chọn giới tính");
    }
  };
  const [birthDay, setBirthday] = useState("");
  const [birthdayError, setBirthdayError] = useState("");
  const onBirthdayChange = (e) => {
    setBirthday(e.target.value);
  };
  const validateBirthday = () => {
    if (birthDay !== "") {
      setBirthdayError("");
    } else {
      setBirthdayError("Vui lòng chọn giới tính");
    }
  };

  const onFormSubmit = (e) => {
    e.preventDefault();
    handleDangKi();
  };
  const handleDangKi = async () => {
    let data = {
      id: 0,
      name: name,
      email: email,
      password: password,
      phone: phone,
      birthday: birthDay,
      gender: true,
      role: "",
    };
    // console.log(data);
    let isValid =
      emailError === "" &&
      passwordError === "" &&
      confirmPasswordError === "" &&
      nameError === "" &&
      phoneError === "" &&
      genderError === "" &&
      birthdayError === "";
    let isEmpty =
      email !== "" &&
      password !== "" &&
      confirmPassword !== "" &&
      name !== "" &&
      phone !== "" &&
      gender !== "" &&
      birthDay !== "";
    if (isEmpty && isValid) {
      try {
        let res = await userService.signUp(data);
        // console.log(res.data.statusCode);
        if (res.data.statusCode === 200) {
          message.success("Tạo tài khoản thành công!");
          navigate("/login");
        }
      } catch (err) {
        console.log(err);
      }
    } else {
      message.error("Lỗi! Vui lòng kiểm tra lại thông tin.");
      return;
    }
  };
  return (
    <div>
      <HomePageNavBar />
      <div className="container mx-auto pt-10">
        <form
          onSubmit={onFormSubmit}
          className="w-full max-w-lg mx-auto shadow-lg p-5"
        >
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full px-3">
              <label className=" block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                Email
              </label>
              <input
                onChange={onEmailChange}
                onBlur={validateEmail}
                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                type="email"
              />
              <p className="text-red-500 text-xs italic">
                {emailError ? emailError : ""}
              </p>
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full px-3">
              <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                Password
              </label>
              <input
                onChange={onPasswordChange}
                onBlur={validatePassword}
                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                type="password"
              />
              <p className="text-red-500 text-xs italic">
                {passwordError ? passwordError : ""}
              </p>
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full px-3">
              <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                Re-enter Password
              </label>
              <input
                onChange={onConfirmPasswordChange}
                onBlur={validateConfirmPassword}
                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                type="password"
              />
              <p className="text-red-500 text-xs italic">
                {confirmPasswordError ? confirmPasswordError : ""}
              </p>
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full px-3 mb-6 md:mb-0">
              <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                Name
              </label>
              <input
                onChange={onNameChange}
                onBlur={validateName}
                className="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                type="text"
              />
              <p className="text-red-500 text-xs italic">
                {nameError ? nameError : ""}
              </p>
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full px-3 mb-6 md:mb-0">
              <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                Phone
              </label>
              <input
                onChange={onPhoneChange}
                onBlur={validatePhone}
                className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                type="text"
              />
              <p className="text-red-500 text-xs italic">
                {phoneError ? phoneError : ""}
              </p>
            </div>
          </div>
          <div className="flex items-center -mx-3 mb-6">
            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
              <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                Gender
              </label>
              <select
                onChange={onGenderSelect}
                onBlur={validateGender}
                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                type="select"
              >
                <option value="">Select...</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
                <option value="private">Prefer not to tell</option>
              </select>
              <p className="text-red-500 text-xs italic">
                {genderError ? genderError : ""}
              </p>
            </div>
            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
              <label
                className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                htmlFor="grid-phone"
              >
                Date of birth
              </label>
              <input
                onChange={onBirthdayChange}
                onBlur={validateBirthday}
                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                type="date"
                value={birthDay}
              />
            </div>
            <p className="text-red-500 text-xs italic">
              {birthdayError ? birthdayError : ""}
            </p>
          </div>
          <div className="text-center">
            <button
              onClick={handleDangKi}
              className="bg-rose-500 text-white hover:bg-rose-600 w-1/3 p-2 rounded"
              type="button"
            >
              Đăng kí
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default SignUp;
