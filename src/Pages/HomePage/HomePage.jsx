import React from "react";
import Footer from "../../Components/Footer/Footer";
import HomePageNavBar from "../../Components/HomePageNavBar/HomePageNavBar";
import NearByLocations from "../../Components/NearByLocations/NearByLocations";
import SearchBar from "../../Components/SearchBar/SearchBar";
import { useMediaQuery } from "react-responsive";
import HomePageBanner from "../../Components/HomePageBanner/HomePageBanner";
import HomePageCarousel from "../../Components/HomePageCarousel/HomePageCarousel";

function HomePage() {
  const isLargeScreen = useMediaQuery({
    query: "(min-width: 640px)",
  });
  return (
    <div className="mx-auto container">
      <HomePageNavBar />
      <SearchBar />
      {isLargeScreen && <HomePageBanner />}
      {!isLargeScreen && <HomePageCarousel />}
      <NearByLocations />
      <Footer />
    </div>
  );
}

export default HomePage;
