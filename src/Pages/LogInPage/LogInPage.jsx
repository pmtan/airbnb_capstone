import { Button } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { setUserInfoStore } from "../../Redux/userSlice";
import { localService } from "../../Services/local.service";
import { userService } from "../../Services/user.service";

function LogInPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const handleLogin = async (e) => {
    e.preventDefault();
    const dataLogin = {
      email: email,
      password: password,
    };
    try {
      let res = await userService.logIn(dataLogin);
      localService.set(res.data.content);
      dispatch(setUserInfoStore(res.data.content.user));
      navigate("/");
      // console.log(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };

  const handleEmail = (e) => {
    setEmail(e.target.value);
  };
  const handlePassword = (e) => {
    setPassword(e.target.value);
  };
  useEffect(() => {
    let userInfoLocal = localService.get();
    if (userInfoLocal) {
      navigate("/");
    }
  }, []);
  return (
    <div className="w-fit mx-auto flex flex-col p-5 items-center justify-center shadow-lg">
      <div
        onClick={() => navigate("/")}
        className="flex items-center justify-center w-full p-5"
      >
        <i className="fab fa-airbnb text-rose-500 text-5xl " />
        <span className="text-rose-500 text-5xl font-semibold pl-2">
          airbnb
        </span>
      </div>
      <form
        onSubmit={handleLogin}
        className="w-full flex flex-col items-center justify-center"
      >
        <div className="w-fit flex pb-6">
          <div className="w-32">
            <label
              className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4"
              htmlFor="inline-full-name"
            >
              Email
            </label>
          </div>
          <div>
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              id="inline-full-name"
              type="email"
              value={email}
              onChange={handleEmail}
            />
          </div>
        </div>
        <div className="w-fit flex mb-6">
          <div className="w-32">
            <label
              className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4"
              htmlFor="inline-password"
            >
              Password
            </label>
          </div>
          <div>
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              id="inline-password"
              type="password"
              value={password}
              onChange={handlePassword}
            />
          </div>
        </div>
        <div className="w-full flex items-center justify-evenly">
          <Button
            onClick={handleLogin}
            size="large"
            className="shadow bg-rose-500 hover:bg-rose-400 focus:shadow-outline focus:outline-none text-white"
            type="button"
          >
            Log In
          </Button>
          <Button
            size="large"
            onClick={() => {
              navigate("/signup");
            }}
            className="shadow bg-rose-500 hover:bg-rose-400 focus:shadow-outline focus:outline-none text-white "
            type="button"
          >
            Sign Up
          </Button>
        </div>
      </form>
    </div>
  );
}

export default LogInPage;
