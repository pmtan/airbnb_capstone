import SecureViewAdmin from "../HOC/SecureViewAdmin";
import SecureViewUser from "../HOC/SecureViewUser";
import AdminPage from "../Pages/AdminPage/AdminPage";
import HomePage from "../Pages/HomePage/HomePage";
import LogInPage from "../Pages/LogInPage/LogInPage";
import RoomDetail from "../Pages/RoomDetail/RoomDetail";
import RoomsAtLocation from "../Pages/RoomsAtLocation/RoomsAtLocation";
import SignUp from "../Pages/SignUp/SignUp";
import UserInfo from "../Pages/UserInfo/UserInfo";

export const routes = [
  {
    path: "/",
    component: <HomePage />,
  },
  {
    path: "/login",
    component: <LogInPage />,
  },
  {
    path: "/signup",
    component: <SignUp />,
  },
  {
    path: "/:city",
    component: <RoomsAtLocation />,
  },
  {
    path: "/room/:id",
    component: <RoomDetail />,
  },
  {
    path: "/user-info",
    component: <SecureViewUser Component={UserInfo} />,
  },
  {
    path: "/admin",
    component: <SecureViewAdmin Component={AdminPage} />,
  },
];
