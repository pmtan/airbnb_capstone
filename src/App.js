import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Spinner from "./Components/Spinner/Spinner";
import { routes } from "./Routes/Routes";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Spinner />
        <Routes>
          {routes.map(({ path, component }) => {
            return <Route key={path} path={path} element={component} />;
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
